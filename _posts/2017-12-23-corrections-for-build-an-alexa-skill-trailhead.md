While working on the [Build an Alexa Skill](https://trailhead.salesforce.com/projects/build-an-alexa-skill/) Trailhead during the #siezethetrail campaign, I ran into a couple of small issues. Each of these issues are outlined below.

## Create a Voice User Interface

In step 10 of the _Amazon Developer Portal_ procedure, the instructions say to download the `InteractionModel.json` file. Unfortunately, the link points to:

```
https://github.com/alexa/skill-sample-nodejs-city-guide/blob/master/speech-assets/InteractionModel.json
```

However, the file is actually located here:

```
https://github.com/alexa/skill-sample-nodejs-city-guide/blob/master/InteractionModel.json
```

## Build the Backend Service

In step 13 of the _Create a Lambda Function_ procedure, you're told to copy and paste the code located at:

```
https://github.com/alexa/skill-sample-nodejs-city-guide/blob/master/src/index.js
```

This file is is really located at:

```
https://github.com/alexa/skill-sample-nodejs-city-guide/blob/master/lambda/custom/index.js
```

Furthermore, in the same step, the _GitHub_ link points to: 

```
https://github.com/alexa/skill-sample-nodejs-city-guide/blob/master/src/index.js
```

but is really located at

```
https://github.com/alexa/skill-sample-nodejs-city-guide/index.js
```

The AWS Lambda UI is slightly different than described in the instructions as well. Instead of the sequence described in steps 16 through 18, you really only need a single click, as shown here:

![AWS Trigger menu](/assets/images/20171223/aws_trigger.png)

## Conclusion

Hopefully this helps you get past that unforseen stumbling block and get back to developing your Alexa skill!
